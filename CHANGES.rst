.. currentmodule:: maquinas

Version 0.3.2.0
-----------------
- Adds Linear Bounded Automata
- It ads its tests


Version 0.1.5.22
-----------------
- Adds to_string for all
- Minor bugs

Version 0.1.5.22
-----------------
- Fixes collections
- Adds to_string
- Documents io
- Documents collections

Version 0.1.5.21
-----------------
- Adds collections
- Adds jflap collections
- Adds load_jflap
- Fixes AST tuple instead of list

Version 0.1.5.18
-----------------
- Fixes minimization
- Adds save_tree_img

Version 0.1.5.17
-----------------
- Adding minimization


Version 0.1.5.14
-----------------
- Fixing documentation
- FIxing minor errors with reductions


Version 0.1.5.12
-----------------

- Fixes save_file
- Fixes load_file with empty cells
- Add test_io case for save_file

Version 0.1.5.9
---------------

- Adiing Alphabet and Language
- Fixing saving gif


Version 0.1.5.7
---------------

- Visualization for TM

Version 0.1.5.6
---------------

- Full documentation
- Testing
- Loading FAs

Version 0.1.5.5
---------------

- Available through pip


