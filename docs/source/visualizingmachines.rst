Visualising machines
====================

.. toctree::
   :maxdepth: 2


Saving visualization to files
-----------------------------

To save a machine into an image file you can use  :func:`~maquinas.regular.dfa.DeterministicFiniteAutomaton.save_img`

.. code-block:: python

  m.save_img(filename,format="jpg")


To create a gif animation of a machine procesing a specific word use :func:`~maquinas.regular.dfa.DeterministicFiniteAutomaton.save_gif`

.. code-block:: python

    m.save_gif(word,filename)

At this moment the filename has to include ".gif" extension.


Visualizing in Notebooks
------------------------

To display a machine into an interactive notebook use  :func:`~maquinas.regular.dfa.DeterministicFiniteAutomaton.graph`

.. code-block:: python

    m.graph()


To simulate a machine into an interactive notebook use :class:`~maquinas.simulation.Simulation`

.. code-block:: python

    from maquinas.simulation import Simulation

    s=Simulation(m,word)
    s.run()


