
Input/output
============

.. toctree::
   :maxdepth: 2

.. module:: maquinas.io

The io module contains...

.. currentmodule:: maquinas.io


load_fa
--------------------

.. autofunction:: maquinas.io.load_fa

load_pda
--------

.. autofunction:: maquinas.io.load_pda

load_tspda
----------

.. autofunction:: maquinas.io.load_tspda

load_mt
-------

.. autofunction:: maquinas.io.load_mt


load_jflap
----------

.. autofunction:: maquinas.io.load_jflap
