Recursively Enumerable  Languages
=================================

.. toctree::
   :maxdepth: 2

.. module:: maquinas

Turing Machine
--------------

.. autoclass:: maquinas.recursivelyenumerable.tm.TuringMachine
   :members:
   :inherited-members:

Two Stack Push Down Automaton
-----------------------------

.. autoclass:: maquinas.recursivelyenumerable.tspda.TwoStackPushDownAutomaton
   :members:
   :inherited-members:
