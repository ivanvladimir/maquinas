Loading machines
================

.. toctree::
   :maxdepth: 2

Regular machines
----------------

Loading a :class:`~maquinas.regular.dfa.DeterministicFiniteAutomaton` or
a :class:`~maquinas.regular.ndfa.NonDeterministicFiniteAutomaton` or
a :class:`~maquinas.regular.ndfa_e.NonDeterministicFiniteAutomaton_epsilon` from a string:

.. code-block:: python

    from maquinas.io import load_fa

    dfa="""
             |  a  | b  |
    -> q0    | q0 | q1 |
       q1  ] | q1 | q0 |
    """

    ndfa="""
             |  a    | b  |
    -> q0    | q0    | q1 |
       q1  ] | q1    |    |
       q2    | q1,q2 | q1,q2  |
    """

    ndfa_e="""
             |  a    | b  | epsilon 
    -> q0    | q0    |    | q2
       q1  ] | q1    | q0 | 
       q2    | q1,q2 | q1,q2  | q2
    """

    m=load_fa(dfa)
    m.print_summary()
    m=load_fa(ndfa)
    m.print_summary()
    m=load_fa(ndfa_e)
    m.print_summary()

Converting machines into strings or saving them into a file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is possible to create a string for the machine:

.. code-block:: python

    from maquinas.regular.dfa import DeterministicFiniteAutomaton as DFA

    m=DFA(Q=['q_0','q_1'],
                         sigma=['a','b'],
                         q_0='q_0',
                         A=['q_0'],
                         delta=[
                            (('q_0','a'),'q_0'),
                            (('q_1','b'),'q_0'),
                            (('q_1','a'),'q_1'),
                            (('q_0','b'),'q_1'),
                         ]) 
    print(m.to_string())
    m.save_file('machine.txt')



Loading machines from `JFLAP <https://www.jflap.org/>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is possible to load FAs using the 'jff' JFLAP format:

.. code-block:: python

    from maquinas.io import load_fa

    jflap_dfa="""
    <structure>
    <type>fa</type>
    <automaton>
    <!--The list of states.-->
    <state id="0" name="q0">
    <x>83.0</x>
    <y>139.0</y>
    <initial/>
    </state>
    <state id="1" name="q1">
    <x>235.0</x>
    <y>61.0</y>
    </state>
    <state id="2" name="q2">
    <x>545.0</x>
    <y>136.0</y>
    </state>
    <state id="3" name="q3">
    <x>391.0</x>
    <y>231.0</y>
    </state>
    <state id="4" name="q4">
    <x>287.0</x>
    <y>285.0</y>
    </state>
    <state id="5" name="q5">
    <x>504.0</x>
    <y>391.0</y>
    </state>
    <state id="6" name="q6">
    <x>161.0</x>
    <y>329.0</y>
    <final/>
    </state>
    <!--The list of transitions.-->
    <transition>
    <from>3</from>
    <to>5</to>
    <read>a</read>
    </transition>
    <transition>
    <from>1</from>
    <to>2</to>
    <read>b</read>
    </transition>
    <transition>
    <from>4</from>
    <to>5</to>
    <read>b</read>
    </transition>
    <transition>
    <from>5</from>
    <to>4</to>
    <read>b</read>
    </transition>
    <transition>
    <from>3</from>
    <to>4</to>
    <read>b</read>
    </transition>
    <transition>
    <from>6</from>
    <to>1</to>
    <read>a</read>
    </transition>
    <transition>
    <from>4</from>
    <to>6</to>
    <read>a</read>
    </transition>
    <transition>
    <from>1</from>
    <to>0</to>
    <read>a</read>
    </transition>
    <transition>
    <from>0</from>
    <to>2</to>
    <read>b</read>
    </transition>
    <transition>
    <from>2</from>
    <to>0</to>
    <read>b</read>
    </transition>
    <transition>
    <from>6</from>
    <to>5</to>
    <read>b</read>
    </transition>
    <transition>
    <from>0</from>
    <to>6</to>
    <read>a</read>
    </transition>
    <transition>
    <from>5</from>
    <to>2</to>
    <read>a</read>
    </transition>
    <transition>
    <from>2</from>
    <to>3</to>
    <read>a</read>
    </transition>
    </automaton>
    </structure>
    """

    m=load_jflap(jflap_dfa)
    m.print_summary()
    


