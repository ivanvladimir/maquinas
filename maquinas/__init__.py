#TODO add machines 
from . import regular, contextfree, contextsensitive, recursivelyenumerable, parser, exceptions

from .simulation import Simulation

__version__ = "0.3.1.5"
