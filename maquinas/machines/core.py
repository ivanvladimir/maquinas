import urllib

class Examples:
    """Class for examples"""

    def __init__(self):
        self.examples = {}

    def list(self):
        """List the names of the examples

        :returns: List of strings
        """
        return list(self.examples.keys())

    def add(
        self, name, type="maquinas", content=None, url=(None, None), description=None
    ):
        """Addes an example to the collection

        :param name: Name of the example
        :param type: Type of the example
        :param content: String of the example
        :param url: Tuple with url of the example, and url which describes the source
        :param descrption: String with descption
        :returns: The example dictionary
        """
        self.examples[name] = {
            "content": content,
            "type": type,
            "url": url[0],
            "url_source": url[1],
            "description": description,
        }
        return self.examples[name]

    def get(self, name):
        """Get the string content from an example

        :param name: Name of the example"""
        if (
            name in self.examples
            and "content" in self.examples[name]
            and "url" in self.examples[name]
            and not self.examples[name]["content"]
        ):
            webf = urllib.request.urlopen(self.examples[name]["url"])
            self.examples[name]["content"] = webf.read()
        if (
            name in self.examples
            and "content" in self.examples[name]
            and self.examples[name]["content"]
        ):
            return self.examples[name]["content"]
